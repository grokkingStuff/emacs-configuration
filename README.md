<h3 align="center">Vishakh's emacs configuration</h3>
<hr/>

<p align="center">
  <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/08/EmacsIcon.svg/120px-EmacsIcon.svg.png" />
</p>

<p align="center">
<a href="https://www.gnu.org/licenses/gpl-3.0"><img src="https://img.shields.io/badge/License-GPL%20v3-blue.svg" alt="GPL License"></a>
</p>

This Emacs configurtion is meant to run on Arch WSL.
(Think terminal emacs with linux utilities but still tied to Windows). 
It's fairly opinionated so buyer beware. 

### How the config structure works

This configuration is split into two parts - 
- `init.el` file that acts as a bootloader for the actual configuration `config.el`. 
   Has a package manager declaration and not much more.
- `config.el` that is my literate file `config.org` tangled by org-babel at the end of `init.el` 
   This allows me to put most of my configuration in fairly 'human' way without worring about preambles and the pre-requisite installation. 

### What about the actual configuration?
Check out `config.org`



